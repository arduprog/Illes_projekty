#include "SimpleTimer.h"
#include "LiquidCrystal_I2C.h"
#include "stopwatch.h"
int counter = 0;
SimpleTimer t;
bool countarm = true;
bool jackarm = true;
bool buttonarm = true;
bool bumm = 0;
bool saved = 1;
unsigned long bumtime = 60L * 3600000L;
//unsigned long bumtime = 6000L;
StopWatch stpw;
bool pressed = 0;
LiquidCrystal_I2C big(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
LiquidCrystal_I2C small(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
void setup() {
  Serial.begin(9600);
  big.begin(20, 4);
  small.begin(16, 2);
  big.backlight();
  small.backlight();
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(32,OUTPUT);
  // put your setup code here, to run once:
  pinMode(26, INPUT_PULLUP);
  pinMode(24, INPUT_PULLUP);
  pinMode(25, INPUT_PULLUP);
  t.setInterval(1000L, counterr);
  t.setInterval(1000L, countdown);
  t.disable(0);
  big.setCursor(6, 1);
  big.print("60:00:00");
  big.setCursor(5, 2);
  big.print("Activated!");
  small.setCursor(7, 0);
  small.print("0");
  digitalWrite(2, LOW);
  digitalWrite(3, HIGH);
  digitalWrite(32,HIGH);
}
unsigned long prevmil1 = 0;
bool bb = 1;
bool str = 0;
unsigned long currmil = 0;
bool cleared = 0;
void loop() {
  // put your main code here, to run repeatedly:
  t.run();
  if (bumm == 1) {
    t.disable(1);
    if (!cleared) {
      big.clear();
      cleared = 1;
    }
    big.setCursor(6, 1);
    big.print("00:00:00");
    big.setCursor(8, 2);
    big.print("BUMM");
  } else {
    if (str == 0) {
      stpw.start();
      // Serial.println("started");
      str = 1;
    }
    if (buttonarm == 0 && countarm == 0 && jackarm == 0 && bumm == 0) {
      stpw.stop();
      big.setCursor(4, 2);
      big.print("Deactivated");
      t.disable(1);
      if(bb) {
        prevmil1 = millis();
        bb = 0;
      } 
      if(millis()-prevmil1 <= 5000) {
        digitalWrite(32,0);
      } else {
        digitalWrite(32,1);
      }
    } else {
      t.enable(1);

    }

    if (countarm == 1) {

      if (digitalRead(26) == LOW && saved == 1) {
        t.enable(0);
        pressed = 1;
      } else {
        pressed = 0;
        t.disable(0);
        if (counter == 21) {
          small.setCursor(2, 0);
          small.print("Wait please!");
          if (saved) {
            currmil = millis();
            saved = 0;
          }
          if (millis() - currmil >= 20000) {
            countarm = 0;
            small.clear();
            small.setCursor(4, 0);
            small.print("Disarmed! ");
            digitalWrite(3, LOW);
            digitalWrite(2, HIGH);
          }
        } else if (counter != 0) {
          small.setCursor(2, 0);
          small.print("Wait please!");
          if (saved) {
            currmil = millis();
            saved = 0;
          }
          if (millis() - currmil >= 20000) {
            small.clear();
            //delay(20000);
            countarm = 1;
            counter = 0;
            small.setCursor(7, 0);
            small.print("0");
            digitalWrite(2, LOW);
            digitalWrite(3, HIGH);
            saved = 1;
          }
        }
      }
    }
    /////////////////////////////////////////////////////////////
    if (jackarm == 1) {
      bool one = 0;
      if (digitalRead(24) == 0) {
        one = 1;
      } else {
        one = 0;
      }
      if (one == 1) {
        jackarm = 0;
        digitalWrite(5, LOW);
        digitalWrite(4, HIGH);
      } else {
        digitalWrite(5, 1);
        digitalWrite(4, 0);
      }
    }
    /////////////////////////////////////////////////////////////////
    if (buttonarm == 1) {
      bool one = 0;
      if (digitalRead(25) == 0) {
        one = 1;
      } else {
        one = 0;
      }
      if (one == 1) {
        buttonarm = 0;
        digitalWrite(7, LOW);
        digitalWrite(6, HIGH);
      } else {
        digitalWrite(7, 1);
        digitalWrite(6, 0);
      }
    }
    /////////////////////////////////////////////////////////////////

  }
}

void counterr() {
  counter = counter + 1;
  small.setCursor(7, 0);
  small.print(counter);
}
void countdown() {
  unsigned long curr = bumtime - stpw.elapsed();
  // Serial.println(curr);
  if (stpw.elapsed() > bumtime) {
    bumm = 1;
  } else {
    bumm = 0;
  }
  byte currH = curr / 3600000L;
  byte currM = (curr - (currH * 3600000L)) / 60000L;
  byte currS = (curr - (currH * 3600000L) - (currM * 60000L)) / 1000L;
  if (currH < 10) {
    big.setCursor(6, 1);
    big.print("0");
    big.print(currH);
  } else {
    big.setCursor(6, 1);
    big.print(currH);
  }
  big.print(":");
  if (currM < 10) {
    big.print("0");
    big.print(currM);
  } else {
    big.print(currM);
  }
  big.print(":");
  if (currS < 10) {
    big.print("0");
    big.print(currS);
  } else {
    big.print(currS);
  }
}

